var i;
var count = 0;
var timer = 0;
var playerCount = 0;
var usingItems = "";
var wolfCount = 0;
var regVillagerCount = 0;
var currentTab = 0;
var villageTeamCount;
var werewolfTeamCount;
var specialTeamCount;
var playersInfoList;
var cardsInUseList;
var werewolfTeam = {
    "Werewolf": -6,
    "WolfCub": -8,
    "Minion": -6,
    "Sorceress": -3,
    "LoneWolf": -5,
    "FangFace": -5,
    "FruitBrute": -3,
    "BigBadWolf": -9,
    "DireWolf": -4,
    "Wolverine": -4
};
var specialTeam = {
    "Vampire": -7,
    "Tanner": -2,
    "Hoodlum": 0,
    "CultLeader": 1
};
var villageTeam = {
    "Villager": 1,
    "Mason": 2,
    "Cursed": -3,
    "VirginiaWoolf": -2,
    "Lycan": -1,
    "OldHag": 1,
    "VillageIdiot": 2,
    "Doppelganger": -2,
    "priest": 3,
    "PI": 3,
    "Cupid": -3,
    "SpellCaster": 1,
    "TroubleMaker": -3,
    "Prince": 3,
    "Pacifist": -1,
    "Witch": 4,
    "BodyGuard": 3,
    "ToughGuy": 3,
    "Seer": 7,
    "ApprenticeSeer": 4,
    "AuraSeer": 3,
    "Drunk": 4,
    "Diseased": 3,
    "Ghost": 2,
    "Mayor": 2,
    "Hunter": 3
};

function navController() {
    if (count == 0) {
        openNav();
        count = 1;
    } else {
        closeNav();
        count = 0;
    }
}

function openNav() {
    document.getElementById("sidebarnav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("sidebarnav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}

function setGameStats() {
    document.getElementById("villagerToWolfTeamCount").innerHTML = "Villager(s) to Wolf(s) " + "<u>" + regVillagerCount + " : " + wolfCount + "</u>";
    document.getElementById("villagerCount").innerHTML = "Normal Villager(s) Left: <u>" + regVillagerCount + "</u>";
    document.getElementById("wolfCount").innerHTML = "Wolf(s) Left: <u>" + wolfCount + "</u>";
}

function openCustomGameMenu() {
    navController();
    removeAllPlayers();
    openWindow("customGameModal");
}

function nextPrev(num){
    // This function will figure out which tab to display
  var x = document.getElementsByClassName("teamTab");
  // Exit the function if any field in the current tab is invalid:
  if (num == 1) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + num;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
      currentTab = 0;
    // ... the form gets submitted:
    document.getElementById("teamTabForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTeamTab(currentTab);
}

function showTeamTab(num){
     // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("teamTab");
  x[num].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (num == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (num == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
}

function teamSelector() {

    showTeamTab(num);
    openWindow("teamRoleSelectionModal");

    //Create villageTeam Options
    for (var i = 0; i <villageTeamCount; i++) {
        var vID = "villageTeamDropdown" + i;
        //Create a dropdown list for the amount of village roles
        var select1 = document.createElement("select");
        select1.class = "input";
        select1.id = vID;
        document.getElementById("villageTeamModalForm").appendChild(select1);

        var br = document.createElement("br");
        document.getElementById("villageTeamModalForm").appendChild(br);

        //Populate the newly created dropdown with the apporpriate list
        for (v in villageTeam) {
            var option = document.createElement("option");
            option.value = v
            option.innerHTML = v;
            document.getElementById(vID).appendChild(option);
        }
    }
    var button1 = document.createElement("button");
    button1.type = "button";
    button1.class = "btn"
    button1.id = "villageTeamModalFormButton";
    button1.innerHTML = "Next Team"
    document.getElementById("villageTeamModalForm").appendChild(button1);

    //Create werewolfTeam Options
    for (var i = 0; i < werewolfTeamCount; i++) {
        var wID = "werewolfTeamDropdown" + i;
        //Create a dropdown list for the amount of village roles
        var select2 = document.createElement("select");
        select2.class = "input";
        select2.id = wID;
        document.getElementById("werewolfTeamModalForm").appendChild(select2);

        var br = document.createElement("br");
        document.getElementById("villageTeamModalForm").appendChild(br);

        //Create werewolfTeamDropdown Options
        for (w in werewolfTeam) {
            var option2 = document.createElement("option");
            option2.value = w
            option2.innerHTML = w;
            document.getElementById(wID).appendChild(option2);
        }
    }
    var button2 = document.createElement("button");
    button2.type = "button";
    button2.class = "btn";
    button2.id = "werewolfTeamModalFormButton";
    button1.innerHTML = "Next Team"
    document.getElementById("werewolfTeamModalForm").appendChild(button2);

     //Create specialTeam Options
    for (var i = 0; i < werewolfTeamCount; i++) {
        var sID = "specialTeamDropdown" + i;
        //Create a dropdown list for the amount of village roles
        var select3 = document.createElement("select");
        select3.class = "input";
        select3.id = sID;
        document.getElementById("specialTeamModalForm").appendChild(select3);

        var br = document.createElement("br");
        document.getElementById("villageTeamModalForm").appendChild(br);

        //Create specialTeamDropdown Options
        for (s in specialTeam) {
            var option3 = document.createElement("option");
            option3.value = s
            option3.innerHTML = s;
            document.getElementById(sID).appendChild(option3);
        }
    }
    var button3 = document.createElement("button");
    button3.type = "button";
    button3.class = "btn"
    button3.innerHTML = "Submit";
    button3.id = "specialTeamModalFormButton";
    document.getElementById("specialTeamModalForm").appendChild(button3);
}

function populatecardsInUseList(dropdownId,teamName,teamCount) {
    cardsInUseList = [];
    var cardName;
    
    for (var i = 0; i < teamCount; i++) {
        cardName = document.getElementById(dropdownId + i).value; 
        cardsInUseList[cardName] = {
            team: teamName,
            balanceNum: i
        };
    }
}

function customGameSetup() {
    timer = document.getElementById("dayPhaseTimer").value;
    playerCount = document.getElementById("playerCount").value;
    villageTeamCount = document.getElementById("villageTeamCount").value;
    werewolfTeamCount = document.getElementById("werewolfTeamCount").value;
    SpecialTeamCount = document.getElementById("SpecialTeamCount").value;
    usingItems = document.getElementById("usingItems").value;
    createJsonPlayerList();
    closeWindow("customGameModal")
    showPlayerCircles();
    addOnclickEventToPlayers();
    createJsonPlayerList();
    teamSelector();

}

//creates nested json object with player # []
function createJsonPlayerList() {
    playersInfoList = [];
    var player = "";
    for (i = 1; i < playerCount + 1; i++) {
        player = "Player" + i;
        playersInfoList[player] = {
            team: "p" + i + "team unknown",
            cardName: "p" + i + " unknown",
            balanceNum: null,
            ability: "p" + i + " unknown"
        };
    }
}

function showPlayerCircles() {
    //Creates a circle of cirlces based on number of players and mkaes them visable on the page
    var div = 360 / playerCount;
    var radius = 150;
    var playerId = "";
    var playerCirles = document.getElementById("playerCircles");
    var offsetToParentCenter = parseInt(playerCirles.offsetWidth / 2); //assumes parent is square
    var offsetToChildCenter = 15;
    var totalOffset = offsetToParentCenter - offsetToChildCenter;
    for (var i = 1; i <= playerCount; ++i) {
        playerId = "Player" + i.toString();
        var childCircles = document.createElement("button");
        childCircles.innerHTML = i.toString();
        childCircles.className = "childCircles";
        childCircles.id = playerId
        var y = Math.sin((div * i) * (Math.PI / 180)) * radius;
        var x = Math.cos((div * i) * (Math.PI / 180)) * radius;
        childCircles.style.top = (y + totalOffset).toString() + "px";
        childCircles.style.left = (x + totalOffset).toString() + "px";
        playerCirles.appendChild(childCircles);
    }
}

function removeAllPlayers() {
    //check if there is at least a player1 then removes all players
    if (document.getElementById("player1")) {
        index = 1;
        var x;
        var player;
        for (i = 0; i < playerCount; i++) {
            player = "player" + index;
            x = document.getElementById(player);
            x.remove(x.selectedIndex);
            index++;
        }
    }
}

function addOnclickEventToPlayers() {
    //add onclick event to player buttons to open player stat window
    var player;
    var window;
    for (var i = 1; i <= playerCount; ++i) {
        player = "Player" + i.toString()
        playerId = document.getElementById(player);
        //playerId.addEventListener("click", function(){openWindow(player + "StatModal")});
        window = player + "StatModal";
        playerId.setAttribute("onclick", "openWindow(\"" + window + "\")");
        //console.log("Made it!")

        createPlayerStatModals(player);
    };
}

function createPlayerStatModals(playerId) {
    //creates playerNModal parent div
    var modalDiv = document.createElement("div");
    modalDiv.className = "modal";
    modalDiv.id = playerId + "StatModal";
    document.getElementById("allPlayerModals").appendChild(modalDiv);

    var modalContentDiv = document.createElement("div");
    modalContentDiv.className = "modal-content";
    modalContentDiv.id = playerId + "-modal-content";
    document.getElementById(playerId + "StatModal").appendChild(modalContentDiv);

    var modalFormHeaderDiv = document.createElement("div");
    modalFormHeaderDiv.className = "modalForm-header";
    modalFormHeaderDiv.id = playerId + "-modalForm-header";
    document.getElementById(playerId + "-modal-content").appendChild(modalFormHeaderDiv);

    var closeSpan = document.createElement("span");
    closeSpan.className = "close";
    closeSpan.innerHTML = "&times;";
    closeSpan.addEventListener("click", function () {
        closeWindow(playerId + "StatModal")
    });
    document.getElementById(playerId + "-modalForm-header").appendChild(closeSpan);

    var playerStatusPanelH2 = document.createElement("h2");
    playerStatusPanelH2.innerHTML = playerId + " Status Panel";
    document.getElementById(playerId + "-modalForm-header").appendChild(playerStatusPanelH2);

    var playerModalForm = document.createElement("form");
    playerModalForm.className = "modalForm";
    playerModalForm.id = playerId + "ModalForm";
    document.getElementById(playerId + "-modalForm-header").appendChild(playerModalForm);

    var cardNameLabel = document.createElement("label");
    cardNameLabel.className = "label";
    cardNameLabel.id = playerId + "cardName";
    cardNameLabel.innerHTML="Role: "
    document.getElementById(playerId + "ModalForm").appendChild(cardNameLabel);

    var playerCardNameLabel = document.createElement("label");
    playerCardNameLabel.className = "label";
    playerCardNameLabel.id = playerId + "CardName";
    playerCardNameLabel.innerHTML = "WereWolf"
    document.getElementById(playerId + "ModalForm").appendChild(playerCardNameLabel);

    var updateRoleBtn = document.createElement("button");
    updateRoleBtn.className = "update-btn";
    updateRoleBtn.id = playerId + "UpdateRole";
    updateRoleBtn.innerHTML = "Update?"
    document.getElementById(playerId + "ModalForm").appendChild(updateRoleBtn);

    var br = document.createElement("br");
    document.getElementById(playerId + "ModalForm").appendChild(br);

    var aliveLabel = document.createElement("label");
    aliveLabel.className = "label";
    aliveLabel.id = playerId + "AliveLabel";
    aliveLabel.innerHTML="Alive: "
    document.getElementById(playerId + "ModalForm").appendChild(aliveLabel);

    var aliveTextLabel = document.createElement("label");
    aliveTextLabel.className = "label";
    aliveTextLabel.id = playerId + "AliveTextLabel";
    aliveTextLabel.innerHTML = "Still Alive"
    document.getElementById(playerId + "ModalForm").appendChild(aliveTextLabel);

    var aliveLabelTextBtn = document.createElement("button");
    aliveLabelTextBtn.type = "button"
    aliveLabelTextBtn.className = "update-btn";
    aliveLabelTextBtn.id = playerId + "AliveBtn";
    aliveLabelTextBtn.innerHTML = "Update?"
    document.getElementById(playerId + "ModalForm").appendChild(aliveLabelTextBtn);
}

function addWerewolf(quantity) {
    for (var i = 0; i < quantity; i++) {
        var wolf = create("wolf");
        wolf.className("wolf");
    }
}

function addVillager(quantity) {
    for (var i = 0; i < quantity; i++) {
        var villager = create("villager");
        villager.className("villager");
    }
}

function balanceGame() {
    //gets all player cards being used in game, check json(s) for card name/value, 
    //adds balance numbers then checks if it equals 0
    //gives possible suggestions for balancing game

}

function changePlayerRole(player) {
    //gets player by id and updates the relavent player button with the new display properties.

}

function openWindow(windowId) {
    var windowToOpen = document.getElementById(windowId);
    windowToOpen.style.display = "block";
}

function closeWindow(windowId) {
    var windowToClose = document.getElementById(windowId);
    windowToClose.style.display = "none";
}

function formOpenClose (windowId1,windowId2){
    closeWindow(windowId1);
    openWindow(windowId2);
}
